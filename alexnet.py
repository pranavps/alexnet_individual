from __future__ import division
import torch
import torchvision
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
from torchvision import transforms, datasets
import torch.nn.functional as F
import torch.nn.init as init
from torch.optim import lr_scheduler as ls
import collections
import time
import argparse
import csv

#Global Variables
batch_size = 128
num_workers = 24
momentum = 0.9
learning_rate = 0.01
weight_decay = 0.0005

	
data_transform = transforms.Compose([
        transforms.Scale(256),
        transforms.CenterCrop(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406],
                             std=[0.229, 0.224, 0.225])
        #The mean and std values are taken from internet for imagenet
    ])

data_transform_flip = transforms.Compose([
        transforms.Scale(256),
        transforms.CenterCrop(256),
        transforms.RandomCrop(224),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406],
                             std=[0.229, 0.224, 0.225])
    ])

root_dir = '/home/cse/btech/cs1140598/'


#parsing arguments
parser = argparse.ArgumentParser(description='Model Parameters')
parser.add_argument('-s', type=int, default=4096)
parser.add_argument('-d', type=int, default=1)
parser.add_argument('-a', type=int, default=1)
parser.add_argument('-p', type=int, default=1)
parser.add_argument('-o', default="AlexNet")
parser.add_argument('-i', type=int, default=0)
parser.add_argument('-w', type=int, default=0)
args = parser.parse_args()

#model's experimental parameters
size_fc = args.s
dropout = (args.d == 1)
activation_relu = (args.a == 1)
overlapping_pooling = (args.p == 1)
optimization_method = args.o
single_iter = (args.i == 1)
initialize_weights = (args.w == 1)

#helper functions

def getDataLoader(folder,transform_input):
	dataset = datasets.ImageFolder(root=root_dir+"dataset/"+folder,
                                           transform=transform_input)
	dataloader = torch.utils.data.DataLoader(dataset,batch_size=batch_size, shuffle=True,
                                             num_workers=num_workers)
	return dataloader

def modelType():
	return "fc_" + str(size_fc) + "_dropout_" + str(dropout) + "_activation_" + \
	("relu" if activation_relu else "tanh") + "_overlapping_pooling_" + str(overlapping_pooling) + \
	"_opti_" + str(optimization_method) + "_single_iter_" + str(single_iter) + "_weights_initialize_" + \
	str(initialize_weights)

def test_accuracy(test_loader,model):
	correct = 0
	total = 0
	model.eval()
	for data in test_loader:
		images, labels = data
		if torch.cuda.is_available():
			images, labels = images.cuda(), labels.cuda()

		images = Variable(images)
		
		outputs = model(images)
		_, predicted = torch.max(outputs.data, 1)
		total += labels.size(0)
		correct += (predicted == labels).sum()

	return (100*(float(correct)/float(total)))


class AlexNet(nn.Module):
	def __init__(self, num_classes = 35, initialize_weights = False, size_fc = 4096, dropout = True, 
		activation_relu = True, overlapping_pooling = True):
		super(AlexNet, self).__init__()
		self.pooling_kernel_size = 3 if overlapping_pooling else 2

		self.dict_conv = collections.OrderedDict()

		self.dict_conv['conv1'] = nn.Conv2d(3, 96, kernel_size=11, stride=4, padding=2) #padding is applied before convolution
		self.dict_conv['activation1'] = nn.ReLU(inplace=True) if activation_relu else nn.Tanh()
		self.dict_conv['pool1'] = nn.MaxPool2d(kernel_size=self.pooling_kernel_size, stride=2)

		self.dict_conv['conv2'] = nn.Conv2d(96, 256, kernel_size=5, padding=2)
		self.dict_conv['activation2'] = nn.ReLU(inplace=True) if activation_relu else nn.Tanh()
		self.dict_conv['pool2'] = nn.MaxPool2d(kernel_size=self.pooling_kernel_size, stride=2)

		self.dict_conv['conv3'] = nn.Conv2d(256, 384, kernel_size=3, padding=1)
		self.dict_conv['activation3'] = nn.ReLU(inplace=True) if activation_relu else nn.Tanh()

		self.dict_conv['conv4'] = nn.Conv2d(384, 384, kernel_size=3, padding=1)
		self.dict_conv['activation4'] = nn.ReLU(inplace=True) if activation_relu else nn.Tanh()

		self.dict_conv['conv5'] = nn.Conv2d(384, 256, kernel_size=3, padding=1)
		self.dict_conv['activation5'] = nn.ReLU(inplace=True) if activation_relu else nn.Tanh()
		self.dict_conv['pool3'] = nn.MaxPool2d(kernel_size=self.pooling_kernel_size, stride=2)
		
		self.convolution = nn.Sequential(self.dict_conv)
		# Code for initialization of weights and biases
		if(initialize_weights):
			init.normal(self.convolution[0].weight, mean=0, std=0.01)
			init.constant(self.convolution[0].bias, 0)
			init.normal(self.convolution[3].weight, mean=0, std=0.01)
			init.constant(self.convolution[3].bias, 1)
			init.normal(self.convolution[6].weight, mean=0, std=0.01)
			init.constant(self.convolution[6].bias, 0)
			init.normal(self.convolution[8].weight, mean=0, std=0.01)
			init.constant(self.convolution[8].bias, 1)
			init.normal(self.convolution[10].weight, mean=0, std=0.01)
			init.constant(self.convolution[10].bias, 1)

		#architecture of linear layers
		self.dict_linear = collections.OrderedDict()
		if(dropout):
			self.dict_linear['dropout1'] = nn.Dropout()
		self.dict_linear['linear1'] = nn.Linear(256 * 6 * 6, size_fc)
		self.dict_linear['activation1'] = nn.ReLU(inplace=True) if activation_relu else nn.Tanh()
		if(dropout):
			self.dict_linear['dropout2'] = nn.Dropout()
		self.dict_linear['linear2'] = nn.Linear(size_fc, size_fc)
		self.dict_linear['activation2'] = nn.ReLU(inplace=True) if activation_relu else nn.Tanh()
		self.dict_linear['linear3'] = nn.Linear(size_fc, num_classes)

		self.linear = nn.Sequential(self.dict_linear)

		if(initialize_weights):
			init.normal(self.linear[1].weight, mean=0, std=0.01)
			init.constant(self.linear[1].bias, 1)
			init.normal(self.linear[4].weight, mean=0, std=0.01)
			init.constant(self.linear[4].bias, 1)
			init.normal(self.linear[6].weight, mean=0, std=0.01)
			init.constant(self.linear[6].bias, 1)

	def forward(self,x):
		x = self.convolution(x)
		x = x.view(x.size(0), 256 * 6 * 6)# here x.size(0) gives the batch size
		x = self.linear(x)
		return x

def train(trainloader, valloader):

	alex_net = AlexNet(initialize_weights = initialize_weights, size_fc = size_fc, dropout = dropout, 
		activation_relu = activation_relu, overlapping_pooling = overlapping_pooling)
	
	if torch.cuda.is_available():
		alex_net = alex_net.cuda()

	alex_net_optimal = AlexNet(initialize_weights = initialize_weights, size_fc = size_fc, dropout = dropout, 
		activation_relu = activation_relu, overlapping_pooling = overlapping_pooling)
	
	if torch.cuda.is_available():
		alex_net_optimal = alex_net_optimal.cuda()
	

	criterion = nn.CrossEntropyLoss()
	
	if optimization_method == "SGD":
		optimizer = torch.optim.SGD(alex_net.parameters(), lr=learning_rate)
	elif optimization_method == "SGDmom":
		optimizer = torch.optim.SGD(alex_net.parameters(), lr=learning_rate, momentum = momentum)
	elif optimization_method == "ADAM":
		optimizer = torch.optim.Adam(alex_net.parameters(), lr=0.0001, weight_decay = weight_decay)
	else:
		optimizer = optim.SGD(alex_net.parameters(), lr = learning_rate, weight_decay = weight_decay, momentum = momentum)
	
	#scheduler is for decreasing learning rate
	scheduler = ls.ReduceLROnPlateau(optimizer,  mode='min', factor=0.1, patience=2)

	writer = csv.writer(open(root_dir+'models/'+modelType()+".csv", "w"))
	writer.writerow(["iteration", "iteration_time", "train_error", "validation_acc"])

	#early stopping parameters
	best_acc = 0.0
	min_delta = 1e-3 #for stable numeric computation
	patience = 10

	epoch = 0
	iteration_num = 0

	#early stopping algorithm
	start_time = time.time()
	running_time = 0
	if(single_iter):
		alex_net.train()
		for i, data in enumerate(trainloader, 0):
			# get the inputs
			inputs, labels = data

			# convert to cuda
			if torch.cuda.is_available():
				inputs = inputs.cuda()
				labels = labels.cuda()

			# wrap them in Variable
			inputs, labels = Variable(inputs), Variable(labels)

			# zero the parameter gradients
			optimizer.zero_grad()

			# forward + backward + optimize
			outputs = alex_net(inputs)
			loss = criterion(outputs, labels)
			loss.backward()
			optimizer.step()


		time_taken = time.time() - start_time
		running_time += time_taken
		start_time = time.time()
		print('Time for iteration is %.5f' % (time_taken))

		val_acc = test_accuracy(valloader,alex_net)

		print('Accuracy of the network on the validation set: %.5f %% ' % (val_acc))
		return alex_net
	else:
		while epoch < patience:
			alex_net.train()
			iteration_num = iteration_num + 1
			
			for i, data in enumerate(trainloader, 0):
				# get the inputs
				inputs, labels = data

				# convert to cuda
				if torch.cuda.is_available():
					inputs = inputs.cuda()
					labels = labels.cuda()

				# wrap them in Variable
				inputs, labels = Variable(inputs), Variable(labels)

				# zero the parameter gradients
				optimizer.zero_grad()

				# forward + backward + optimize
				outputs = alex_net(inputs)
				loss = criterion(outputs, labels)
				loss.backward()
				optimizer.step()

			time_taken = time.time() - start_time
			running_time += time_taken
			start_time = time.time()
			print('Time for %d iteration is %.5f' % (iteration_num, time_taken))

			val_acc = test_accuracy(valloader,alex_net)

			print('Accuracy of the network on the validation set: %.5f %% ' % (val_acc))
			
			#writing to csv
			writer.writerow([iteration_num, time_taken, test_accuracy(trainloader,alex_net), val_acc])

			scheduler.step(100-val_acc)

			if (val_acc - best_acc) > min_delta:
				best_acc = val_acc
				epoch = 0
				alex_net_optimal.load_state_dict(alex_net.state_dict())
			else:
				epoch = epoch +1

	print('Total running time is %d seconds' % (running_time))
	return alex_net_optimal



if __name__ == '__main__':
	trainloader = getDataLoader('train', data_transform_flip)
	valloader = getDataLoader('validation', data_transform)
	testloader = getDataLoader('test', data_transform)
	print(modelType())
	model = train(trainloader,valloader)

	acc = test_accuracy(valloader,model)
	print('Accuracy of the network on the validation set: %f %%' % (
    	acc))
	
	acc = test_accuracy(testloader,model)
	print('Accuracy of the network on the test set: %f %%' % (
    	acc))
	
	#testtime for an example
	test_start = time.time()
	examples = 0
	for data in trainloader:
		# get the inputs
		images, labels = data
		if torch.cuda.is_available():
			images, labels = images.cuda(), labels.cuda()

		images = Variable(images)
		
		outputs = model(images)
		_, predicted = torch.max(outputs.data, 1)
		examples += labels.size(0)
		break;

	print("Test time for an example is %.5f" % ((float(time.time() - test_start))/float(examples)) )

	torch.save(model.state_dict(),root_dir+'models/' + modelType() + ".pth")
