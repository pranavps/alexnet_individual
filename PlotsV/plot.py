import matplotlib as mpl
import numpy as np
import matplotlib.pyplot as plt
import argparse

#parsing arguments
parser = argparse.ArgumentParser(description='Model Parameters')
parser.add_argument('-f', default="file.csv")
args = parser.parse_args()

#model's experimental parameters
file_name = args.f

data = np.genfromtxt(file_name, delimiter=',', skip_header=1, skip_footer=0,
                    names=['iter', 'iter_time', 'train_acc', 'valid_acc'])

fig = plt.figure()

plt.title("Validation accuracy plot")
plt.xlabel('epochs')
plt.ylabel('Validation Accuracy')

plt.plot(data['iter'], data['valid_acc'])

plt.savefig(file_name + '.png')
