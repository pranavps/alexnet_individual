from __future__ import division
import os
import shutil
import copy
import torch
import torchvision
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
from torchvision import transforms, datasets
import torch.nn.functional as F
import torch.nn.init as init
from torch.optim import lr_scheduler as ls
import collections
import time
import argparse
import csv

	
data_transform = transforms.Compose([
        transforms.Scale(256),
        transforms.CenterCrop(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406],
                             std=[0.229, 0.224, 0.225])
        #The mean and std values are taken from internet for imagenet
    ])

root_dir = '/home/cse/btech/cs1140598/'
batch_size = 128
num_workers = 24

#parsing arguments
parser = argparse.ArgumentParser(description='Test Script args')
parser.add_argument('-l', default="test")
parser.add_argument('-m', default="model.pth")
args = parser.parse_args()

folder_name = args.l
model_path = args.m

#helper functions

def extract_data(folder_name):
	if(os.path.exists(folder_name)):
		label_files = os.listdir(folder_name)
		for t in label_files:
			src = folder_name+'/'+t+'/test'
			if os.path.isdir(src):
				src_files = os.listdir(src)
				directory = root_dir+'dataset/testNew/'+t+'/'
				os.makedirs(directory)
				for file_name in src_files:
				    full_file_name = os.path.join(src, file_name)
				    dest = directory+file_name
				    if (os.path.isfile(full_file_name)):
				        shutil.copy(full_file_name, dest)

def getDataLoader(folder,transform_input):
	dataset = datasets.ImageFolder(root=root_dir+"dataset/"+folder,
                                           transform=transform_input)
	dataloader = torch.utils.data.DataLoader(dataset,batch_size=batch_size, shuffle=True,
                                             num_workers=num_workers)
	return dataloader

def test_accuracy(test_loader,model):
	correct = 0
	total = 0
	model.eval()
	for data in test_loader:
		images, labels = data
		if torch.cuda.is_available():
			images, labels = images.cuda(), labels.cuda()

		images = Variable(images)
		
		outputs = model(images)
		_, predicted = torch.max(outputs.data, 1)
		total += labels.size(0)
		correct += (predicted == labels).sum()

	return (100*(float(correct)/float(total)))


class AlexNet(nn.Module):
	def __init__(self, num_classes = 35, initialize_weights = False, size_fc = 4096, dropout = True, 
		activation_relu = True, overlapping_pooling = True):
		super(AlexNet, self).__init__()
		self.pooling_kernel_size = 3 if overlapping_pooling else 2

		self.dict_conv = collections.OrderedDict()

		self.dict_conv['conv1'] = nn.Conv2d(3, 96, kernel_size=11, stride=4, padding=2) #padding is applied before convolution
		self.dict_conv['activation1'] = nn.ReLU(inplace=True) if activation_relu else nn.Tanh()
		self.dict_conv['pool1'] = nn.MaxPool2d(kernel_size=self.pooling_kernel_size, stride=2)

		self.dict_conv['conv2'] = nn.Conv2d(96, 256, kernel_size=5, padding=2)
		self.dict_conv['activation2'] = nn.ReLU(inplace=True) if activation_relu else nn.Tanh()
		self.dict_conv['pool2'] = nn.MaxPool2d(kernel_size=self.pooling_kernel_size, stride=2)

		self.dict_conv['conv3'] = nn.Conv2d(256, 384, kernel_size=3, padding=1)
		self.dict_conv['activation3'] = nn.ReLU(inplace=True) if activation_relu else nn.Tanh()

		self.dict_conv['conv4'] = nn.Conv2d(384, 384, kernel_size=3, padding=1)
		self.dict_conv['activation4'] = nn.ReLU(inplace=True) if activation_relu else nn.Tanh()

		self.dict_conv['conv5'] = nn.Conv2d(384, 256, kernel_size=3, padding=1)
		self.dict_conv['activation5'] = nn.ReLU(inplace=True) if activation_relu else nn.Tanh()
		self.dict_conv['pool3'] = nn.MaxPool2d(kernel_size=self.pooling_kernel_size, stride=2)
		
		self.convolution = nn.Sequential(self.dict_conv)
		# Code for initialization of weights and biases
		if(initialize_weights):
			init.normal(self.convolution[0].weight, mean=0, std=0.01)
			init.constant(self.convolution[0].bias, 0)
			init.normal(self.convolution[3].weight, mean=0, std=0.01)
			init.constant(self.convolution[3].bias, 1)
			init.normal(self.convolution[6].weight, mean=0, std=0.01)
			init.constant(self.convolution[6].bias, 0)
			init.normal(self.convolution[8].weight, mean=0, std=0.01)
			init.constant(self.convolution[8].bias, 1)
			init.normal(self.convolution[10].weight, mean=0, std=0.01)
			init.constant(self.convolution[10].bias, 1)

		#architecture of linear layers
		self.dict_linear = collections.OrderedDict()
		if(dropout):
			self.dict_linear['dropout1'] = nn.Dropout()
		self.dict_linear['linear1'] = nn.Linear(256 * 6 * 6, size_fc)
		self.dict_linear['activation1'] = nn.ReLU(inplace=True) if activation_relu else nn.Tanh()
		if(dropout):
			self.dict_linear['dropout2'] = nn.Dropout()
		self.dict_linear['linear2'] = nn.Linear(size_fc, size_fc)
		self.dict_linear['activation2'] = nn.ReLU(inplace=True) if activation_relu else nn.Tanh()
		self.dict_linear['linear3'] = nn.Linear(size_fc, num_classes)

		self.linear = nn.Sequential(self.dict_linear)

		if(initialize_weights):
			init.normal(self.linear[1].weight, mean=0, std=0.01)
			init.constant(self.linear[1].bias, 1)
			init.normal(self.linear[4].weight, mean=0, std=0.01)
			init.constant(self.linear[4].bias, 1)
			init.normal(self.linear[6].weight, mean=0, std=0.01)
			init.constant(self.linear[6].bias, 1)

	def forward(self,x):
		x = self.convolution(x)
		x = x.view(x.size(0), 256 * 6 * 6)# here x.size(0) gives the batch size
		x = self.linear(x)
		return x


if __name__ == '__main__':

	extract_data(root_dir+folder_name)
	
	testloader = getDataLoader('testNew', data_transform)

	model  = AlexNet(size_fc = 256)
	if torch.cuda.is_available():
		model = model.cuda()
	model.load_state_dict(torch.load(model_path))

	acc = test_accuracy(testloader,model)
	print('Accuracy of the network on the test set: %f %%' % (acc))
